# PXL Web & Mobile project - Individual case
This project contains three different branches. These branches contain code for a very basic forum.  
It is a **purely educational project** so the forum is far from perfect.

**Branches**
- Master: SQL script for the database
- WP1: PHP backend without doctrine
- WP2: React frontend and PHP backend from WP1
- WP3: PHP backend with doctrine and bootstrap as frontend

# The branches in detail

### WP1
**Description**
- REST interface in Symfony4 without Doctrine
- [Email input validation](https://github.com/nojacko/email-validator)

**Progress**
- [X] Add three extra routes
- [X] Email input validation  
- [X] 100% testcoverage  

**Install**  
Run the [SQL script](https://gitlab.com/RobinWils/pxl-web-and-mobile-react/raw/master/bulletinboard.sql?inline=false) to create your database.  
```cd bulletinboard```  
```composer install```  
```php bin/console server:start```

### WP2
**Description**
- React frontend (Material-UI + Loading bar from Material Design Lite)  
- Backend from WP1
- [react-markdown](https://github.com/rexxars/react-markdown)

**Progress**
- [X] React-markdown
- [X] [ESLint](https://github.com/standard/eslint-config-standard-react)
- [X] One new page

**Install**  
Run the [SQL script](https://gitlab.com/RobinWils/pxl-web-and-mobile-react/raw/master/bulletinboard.sql?inline=false) to create your database.  
```cd bulletinboard```  
```composer install```  
```cd frontend-bulletinboard```  
```npm install```  
```npm start```

### WP3
**Description**
- Bootstrap frontend
- PHP with doctrine backend
- Email symfony package  
```composer require symfony/swiftmailer-bundle```

**Install**  
```cd bulletinboard```  
```composer install```  
```php bin/console doctrine:database:create```  
```php bin/console make:migration```  
```php bin/console doctrine:migrations:migrate```

Insert data into the database.  
Insert admin user with the password and username 'admin'.    
```INSERT INTO user (id, username, roles, password, email) VALUES (1, 'admin', 'a:1:{i:0;s:10:"ROLE_ADMIN";}', '$argon2i$v=19$m=1024,t=2,p=2$a2NBbkVZd1c4ZEJEdlA4NQ$QS21IOrv63uz3Cr1GGvjJwlxMSiuawlnAnLDtq2dZt4', 'example@gmail.com');```

Insert a demo post.  
```INSERT INTO post (id, title, content, category, user_id, upvotes, downvotes) VALUES (1, 'First post', 'My first content.', 'My first category', 1, 0, 0);```

Insert two reactions.  
```INSERT INTO reaction (id, content, token, post_id) VALUES (1, 'First reaction', 'ou7oeuh7oe9uh7o82pu312.', 1); INSERT INTO reaction (id, content, token, post_id) VALUES (2, 'Second reaction', 'oeuouh7euoh7o82pu913.', 1);```

```php bin/console server:start```

**Progress**
- [X] Symfony swiftmailer-bundle
- [X] Responsive frontend
- [ ] Ten functional tests
- [ ] Symfony best practices

# License
### WTFPL
This project is **NOT free software** and is **licensed under the WTFPL** license.  
This means that you can do whatever you like with it. You can steal parts of this project or even this whole project if you wish to do so. I picked the WTFPL license since the project is purely educational.

### Why is it not GPL licensed?
It is not GPL since that would cost me more time and the chance is high that some of the included frameworks or libraries make use of proprietary software. I do not think that this project had no value. It was a good project to find out how the React library and the Symfony framework work.

# Thanks!
Thank you for checking out this project.  
I hope that some of the code might be useful.  

### My opinion
I personally would not use most of these tools in my own projects since I care about free software.  
I don't like JavaScript since many associate it with [The JavaScript Trap](https://www.gnu.org/philosophy/javascript-trap.html).